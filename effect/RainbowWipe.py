#!/usr/bin/env python3

import socket
import struct
import colorsys
import time

from NetPixel import NetPixel

def hsv2rgb(h,s,v):
    return tuple(round(i * 255) for i in colorsys.hsv_to_rgb(h,s,v))

strip = NetPixel("192.168.1.130")

while True:
    t = int(round(time.time() * 250))
    for i in range(strip.length):
        strip[i] = hsv2rgb((t + i * 15) / 1000, 1, 0.1)
    strip.write()
