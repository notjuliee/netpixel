import socket, struct

from typing import Tuple, List

Color = Tuple[int, int, int]
RECV_SIZE = 256

class RequestType:
    INFO: int = 0x01
    FILL: int = 0x02
    SET:  int = 0x03

class NetPixel:
    def __init__(self, server: str, port: int=23) -> None:
        self.autowrite = False

        self._server = server
        self._port = port

        self._s: socket.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self._s.connect((server, port))

        self.name, self.length = struct.unpack("64sB", self._send([RequestType.INFO]))

        self._buf: List[Color] = [(0, 0, 0)] * self.length

    def __str__(self):
        return f"Strip {self.name} at {self._server}:{self._port}"

    def __repr__(self):
        return f"{self.__class__.__name__}({self._server}, {self._port})"

    def _send(self, data: bytearray) -> bytearray:
        self._s.send(bytearray(data))
        return self._s.recv(RECV_SIZE)

    def __setitem__(self, index: int, val: Color) -> None:
        self._buf[index] = val
        if self.autowrite:
            self.write()

    def __getitem__(self, index: int) -> Color:
        return self._buf[index]

    def write(self) -> None:
        wbuff: bytearray = bytearray([RequestType.SET])

        for x in self._buf:
            wbuff += bytearray(x)

        self._send(wbuff)

    def fill(self, val: Color, start: int=0, length: int=-1) -> None:
        if length == -1:
            length = self.length
        self._buf[start:start+length] = [val] * (length)
        if self.autowrite:
            self.write()

class SubStrip:
    def __init__(self, strip: NetPixel, start: int, length: int):
        if start + length > strip.length:
            raise IndexError
        self._s: NetPixel = strip
        self._start: int = start
        self.length: int = length

    def __setitem__(self, index: int, val: Color) -> None:
        if index >= self.length:
            raise IndexError
        self._s[self._start + index] = val

    def __getitem__(self, index: int) -> Color:
        if index >= self.length:
            raise IndexError
        return self._s[self._start + index]

    def fill(self, color: Color, start: int=0, length: int=-1):
        self._s.fill(color, self._start + start, length)

