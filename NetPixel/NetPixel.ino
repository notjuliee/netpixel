#include <ESP8266WiFi.h>
#include <FastLED.h>

#define WIFI_SSID "{SSID}"
#define WIFI_PSK "{PASSWORD}"
#define NAME "DESK_STRIP"
#define LENGTH 131

struct ServerInfo {
    char name[64];
    uint8_t length;
};
struct ServerInfo s_info;

WiFiServer server(23);
WiFiClient client;

uint8_t message_buffer[1024];
// Add one extra LED for padding
CRGB leds[LENGTH + 1];

void setup() {
    Serial.begin(115200);
    Serial.println("\n\nStarting NetPixel");
    Serial.println("Waiting for connection");

    WiFi.begin(WIFI_SSID, WIFI_PSK);
    while (WiFi.status() != WL_CONNECTED) {
        delay(500);
        Serial.print(".");
    }

    Serial.println("\nConnected to network");
    Serial.print("Our IP is ");
    Serial.println(WiFi.localIP());

    memcpy(s_info.name, NAME, sizeof(NAME));
    s_info.length = LENGTH;

    FastLED.addLeds<NEOPIXEL, 4>(leds, LENGTH);
    fill_solid(&(leds[0]), LENGTH, CRGB(32, 0, 32));
    FastLED.show();

    server.begin();
}

uint8_t *BUF_START = &message_buffer[0];
uint8_t *cur_ptr;

void loop() {
    client = server.available();
    while (client.connected()) {
        cur_ptr = BUF_START;
        while (client.available()) {
            *cur_ptr = client.read();
            if (i == BUF_START && *cur_ptr == 0x03) {
                cur_ptr = (uint8_t *)&leds - 1;
            }
            cur_ptr++;
        }
        if (cur_ptr != BUF_START) {
            if (message_buffer[0] == 0x01) {
                client.write((uint8_t *)&s_info, sizeof(struct ServerInfo));
            }
            if (message_buffer[0] == 0x02) {
                fill_solid(&(leds[0]), LENGTH, *(CRGB *)&message_buffer[1]);
                FastLED.show();
                client.print(1);
            }
            if (message_buffer[0] == 0x03) {
                FastLED.show();
                client.print(1);
            }
        }
    }
}
